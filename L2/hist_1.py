#!/bin/bash

import matplotlib.pyplot as plt
import numpy as np

data = [7, 9, 27, 28, 55, 45, 34, 65, 54, 67, 34, 23, 24, 66, 53, 45, 44, 88, 22, 33, 55, 35, 33, 37, 47, 41,31, 30, 29, 1]

binwidth = 10

ax =plt.subplot(1,1,1)

ax.hist(data, bins=int(30/binwidth), color='blue', edgecolor= 'black')

plt.grid(axis='y', alpha=0.75)
plt.xlabel('Value')
plt.ylabel('Freq')
plt.title('Age distribution')
plt.show()

\babel@toc {english}{}
\contentsline {chapter}{\numberline {1}Question 1}{4}{chapter.1}
\contentsline {section}{\numberline {1.1}Decision Tree Induction}{4}{section.1.1}
\contentsline {subsection}{\numberline {1.1.1}Description}{4}{subsection.1.1.1}
\contentsline {subsection}{\numberline {1.1.2}ID3 decision tree learning algorithm}{5}{subsection.1.1.2}
\contentsline {subsection}{\numberline {1.1.3}Example}{6}{subsection.1.1.3}
\contentsline {section}{\numberline {1.2}Back Propagation Network}{8}{section.1.2}
\contentsline {subsection}{\numberline {1.2.1}Algorithm}{8}{subsection.1.2.1}
\contentsline {subsection}{\numberline {1.2.2}Example}{10}{subsection.1.2.2}
\contentsline {section}{\numberline {1.3}Naive Bayesian Classifier}{15}{section.1.3}
\contentsline {subsection}{\numberline {1.3.1}Description}{15}{subsection.1.3.1}
\contentsline {subsection}{\numberline {1.3.2}Naive Bayes Algorithm}{16}{subsection.1.3.2}
\contentsline {subsection}{\numberline {1.3.3}Example}{17}{subsection.1.3.3}
\contentsline {section}{\numberline {1.4}Nearest Neighbor Classifier}{18}{section.1.4}
\contentsline {subsection}{\numberline {1.4.1}Description}{18}{subsection.1.4.1}
\contentsline {subsection}{\numberline {1.4.2}Algorithm}{18}{subsection.1.4.2}
\contentsline {subsection}{\numberline {1.4.3}Example}{19}{subsection.1.4.3}
\contentsline {chapter}{\numberline {2}Question 2}{20}{chapter.2}
\contentsline {section}{\numberline {2.1}Simple Genetic Algorithm}{20}{section.2.1}
\contentsline {chapter}{\numberline {3}Question 5}{21}{chapter.3}
\contentsline {section}{\numberline {3.1}Definitions}{21}{section.3.1}
\contentsline {section}{\numberline {3.2}Example}{22}{section.3.2}
\contentsline {chapter}{\numberline {4}Question 6}{23}{chapter.4}
\contentsline {section}{\numberline {4.1}K Means Clustering}{23}{section.4.1}
\contentsline {subsection}{\numberline {4.1.1}Description}{23}{subsection.4.1.1}
\contentsline {subsection}{\numberline {4.1.2}Algorithm}{23}{subsection.4.1.2}
\contentsline {subsection}{\numberline {4.1.3}Example}{24}{subsection.4.1.3}
\contentsline {section}{\numberline {4.2}K Medoids Clustering}{25}{section.4.2}
\contentsline {subsection}{\numberline {4.2.1}Description}{25}{subsection.4.2.1}
\contentsline {subsection}{\numberline {4.2.2}Algorithm}{25}{subsection.4.2.2}
\contentsline {subsection}{\numberline {4.2.3}Example}{26}{subsection.4.2.3}
\contentsline {section}{\numberline {4.3}Hierarchical Clustering}{27}{section.4.3}
\contentsline {subsection}{\numberline {4.3.1}Description}{27}{subsection.4.3.1}
\contentsline {subsection}{\numberline {4.3.2}Algorithm}{28}{subsection.4.3.2}
\contentsline {subsection}{\numberline {4.3.3}Example}{28}{subsection.4.3.3}
\contentsline {chapter}{\numberline {5}Question 7}{30}{chapter.5}
\contentsline {section}{\numberline {5.1}Cosine}{30}{section.5.1}
\contentsline {subsection}{\numberline {5.1.1}Example}{30}{subsection.5.1.1}
\contentsline {section}{\numberline {5.2}Jaccard}{30}{section.5.2}
\contentsline {subsection}{\numberline {5.2.1}Example}{30}{subsection.5.2.1}
\contentsline {section}{\numberline {5.3}Minkowski}{31}{section.5.3}
\contentsline {subsection}{\numberline {5.3.1}Example}{31}{subsection.5.3.1}
\contentsline {section}{\numberline {5.4}Manhattan}{31}{section.5.4}
\contentsline {subsection}{\numberline {5.4.1}Example}{32}{subsection.5.4.1}
\contentsline {section}{\numberline {5.5}Mahalanobis}{32}{section.5.5}
\contentsline {subsection}{\numberline {5.5.1}Example}{32}{subsection.5.5.1}
\contentsline {chapter}{\numberline {6}Question 8}{34}{chapter.6}

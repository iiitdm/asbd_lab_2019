# $example on$
from pyspark.ml.fpm import FPGrowth
# $example off$
from pyspark.sql import SparkSession

def get_transactions(dataset):
        in_file = open(dataset, "r")
        if_lines = in_file.readlines()
        transactions = []
        for lines in if_lines[:50]:
            string_lines = lines.split()
            int_lines = list(map(int, string_lines))
            int_lines.sort()
            # order_set = OrderedSet(string_lines)
            transactions.append(int_lines)
        
        return transactions


if __name__ == "__main__":
    spark = SparkSession\
        .builder\
        .appName("FPGrowthExample")\
        .getOrCreate()

    transactions = get_transactions("kosarak.dat")

    print(transactions)
    spark_df_list = []
    for i in range(len(transactions)):
        spark_df_list.append((i, transactions[i]))

    print(spark_df_list)
    
    name_list = ['id', 'items']

    # $example on$
    df = spark.createDataFrame(spark_df_list, name_list)
    fpGrowth = FPGrowth(itemsCol="items", minSupport=0.5, minConfidence=0.6)
    model = fpGrowth.fit(df)

    # Display frequent itemsets.
    model.freqItemsets.show()

    # Display generated association rules.
    model.associationRules.show()

    # transform examines the input items against all the association rules and summarize the
    # consequents as prediction
    model.transform(df).show()
    # $example off$

    spark.stop()

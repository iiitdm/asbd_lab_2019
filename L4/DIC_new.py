"""
    some functions here are an inspiration from
    guptaanmol184 repo big-data-lab
"""

ITEM = 0
# INDEX = 1
# TYPE = 2
COUNTER = 1
CHK_TRANS = 2

import itertools
import copy

def create_itemset(item):

    ItemSet = []
    counter = 0
    chk_trans = 0
    ItemSet.append(item)
    # ItemSet.append(dic_index)
    # ItemSet.append("DC")
    ItemSet.append(counter)
    ItemSet.append(chk_trans)
    

    return ItemSet



def print_itemset(itemset):
    print("itemset:", itemset[0], "counter", itemset[1],"transactions",itemset[2])

def print_list(type):
    for x in type:
        print_itemset(x)

class DIC(object):

    def __init__(self, dataset, M, minsup=2):
        self.M = M
        self.SS =[] #solid square
        self.SC =[] #solid cirle
        self.DS =[] #dashed square
        self.DC =[] #dashed circle
        self.transactions = self.get_transactions(dataset)
        self.unique_item_set = self.get_one_itemset()
        self.minsup = minsup
    


    def get_one_itemset(self):
        unique_set = set()
        count = 0
        for line in self.transactions:
            for item in line:
                # print(item)
                new_item = frozenset([item])
                if  new_item not in unique_set:
                    # self.DC.append(ItemSet(item))
                    self.DC.append(create_itemset({item}))
                    count +=1
                unique_set.add(frozenset([item]))
        
        return unique_set
    
    def get_transactions(self, dataset):
        in_file = open(dataset, "r")
        if_lines = in_file.readlines()
        transactions = []
        for lines in if_lines[:50]:
            string_lines = lines.split()
            int_lines = list(map(int, string_lines))
            int_lines.sort()
            transactions.append(int_lines)
        return transactions

    def subset_gen(self, S, n):
        """
        takes in a set S and size of the subsets to generate.
        generates all subsets of size len(s)-1"""
        #print(S)
        a = itertools.combinations(S,n)
        result = []
        for i in a:
            result.append(set(i))
        
        
        # print("Set and subsets",S,result)
        return(result)
    
    def superset_gen(self, S):

        result = []
        a = set()
        for item in self.unique_item_set:
            if item.intersection(S) == set():
                a = item.union(S)
                result.append(a)
                a = set()
        
        return result
    
    def immidiate_checker(self, c):
        # temp_set = frozenset([c[ITEM]])
        sc_set = self.superset_gen(c[ITEM])
        new_DC_set = []
        for sc in sc_set:
            sc_subsets = self.subset_gen(sc, len(sc) -1)
            add_SS =True
            add_DS =True
            for subs in sc_subsets:
                # new_item = create_itemset(list(subs)[0])
                # # unions other subsets in to this item
                # print("before union")
                # print(new_item,sc_subsets,subs)
                # print("union")
                # for x in range(len(subs)):
                #     new_item[ITEM].union(frozenset([list(subs)[x]]))
                #     print(new_item)
                new_item = create_itemset(subs)

                print("DS in imm")
                print([x[ITEM] for x in self.DS])
                if new_item[ITEM] not in [x[ITEM] for x in self.SS]:
                    add_SS = False
                    
                
                if new_item[ITEM] not in [x[ITEM] for x in self.DS]:
                    add_DS = False
                # print("C", c, "superset",sc_set, "subset", sc_subsets, "new item", new_item)
                # print_itemset(new_item)
                                    
            if add_SS or add_DS:
                new_item = create_itemset(set(list(sc)))
                # # unions other subsets in to this item
                # for x in sc:
                #     # print(frozenset([list(sc)[x]]))
                #     print(x)
                #     new_item[ITEM].union(frozenset([x]))
                #     print(new_item)
                
                # print_itemset(new_item)
                
                if new_item[ITEM] not in [x[ITEM] for x in self.DC]:
                    self.DC.append(new_item)
                    print("appending", new_item)
                


    def impl_dic(self):
        trans_len = len(self.transactions)

        for x in self.DC:
            print_itemset(x)
        
        while len(self.DC) != 0 or len(self.DS) != 0:
            for m_trans in range(trans_len):

                for t in self.transactions[m_trans: m_trans+self.M]:
                    for c in self.DC:
                        # print(type(c[ITEM]))
                        # temp_set = frozenset([])
                        if c[ITEM].issubset(frozenset(t)):
                            c[COUNTER] +=1
                        c[CHK_TRANS] +=1
                    

                    # keys_to_del = []
                    # to_add = []
                    for c in self.DS:
                        if c[ITEM].issubset(frozenset(t)):
                            c[COUNTER] +=1
                        c[CHK_TRANS] +=1

                    if m_trans:
                        print(m_trans/m_trans)
                    else:
                        print(m_trans)
                    print("DC")
                    print_list(self.DC)
                    print("DS")
                    print_list(self.DS)


                for c in copy.copy(self.DC):
                    if(c[COUNTER]>=self.minsup):
                        self.DS.append(c)
                        self.DC.remove(c)
                        self.immidiate_checker(c)

                # print("DS moving from DC")
                # print_list(self.DS)

                for c in copy.copy(self.DS):
                    if(c[CHK_TRANS]==trans_len):
                        self.SS.append(c)
                        self.DS.remove(c)
                # print("SS")
                # print_list(self.SS)    			
                for c in copy.copy(self.DC):
                    if(c[CHK_TRANS]==trans_len):
                        self.SC.append(c)
                        self.DC.remove(c)
                # print("SC")
                # print_list(self.SC)
                    


                    #     if c[COUNTER] >= self.minsup:
                    #         c[TYPE] = "DS"
                    #         c[INDEX] = key
                    #         keys_to_del.append(key)
                    #         self.DS[key] = c
                    #         to_add += self.immidiate_checker(c)
                    
                    # for x in to_add:
                    #     x[INDEX] = len(self.DC)+1
                    #     self.DC[len(self.DC)+1] = x
                if m_trans + self.M >trans_len:
                    m_trans = 0
                else:
                    m_trans +=self.M
        print("Final DS")
        print_list(self.DS)
        print("Final DC")
        print_list(self.DC)
        print("Final SC")
        print_list(self.SC)
        print("Final SS")
        print_list(self.SS)


#data set, stride, minsup
my_dic = DIC( "my_set.txt", 5, 2)

my_dic.impl_dic()
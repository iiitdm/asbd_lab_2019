# import collection from defaultdict
from collections import defaultdict



class Apriori(object):

    def __init__(self, min_sup, min_conf):
        self.min_conf = min_conf
        self.min_sup = min_sup
    
    def get_transactions(self, dataset):
        in_file = open(dataset, "r")
        if_lines = in_file.readlines()
        transactions = []
        # transactions_count = {}
        for lines in if_lines[:50]:
            string_lines = lines.split()
            int_lines = list(map(int, string_lines))
            int_lines.sort()
            # transactions.append((int_lines, len(int_lines)))
            transactions.append(int_lines)
            # transactions_count[set(int_lines)] = len(int_lines)
        
        return transactions
    
    def get_itemsets(self, transactions):

        itemset = defaultdict(int)
        count = 0
        for line in transactions:
            for item in line:
                new_set = frozenset([item])
                if new_set not in itemset.values():
                    itemset[count] = new_set #frozenset([item])
                    count +=1

            # for item in line:
            #     itemset.add(frozenset([item]))
        
        return itemset
    
    def get_join_set(self, termSet, k):
        """ Generate new k-terms candiate itemset"""
        # print(type(termSet))

        local_set = defaultdict(int)
        count = 0

        for key1,term1 in termSet.items():
            for key2,term2 in termSet.items():
                if len(term1.union(term2)) ==k:
                    joinset = term1.union(term2)
                    if not joinset in local_set.values():
                        local_set[count] = joinset
                        count+=1
        # print("LOCAL SET")
        # print(local_set)

        return(local_set)
        # local_set

        # print()


        # return set([term1.union(term2) for key1,term1 in termSet.items() for key2,term2 in termSet.items() 
                    # if len(term1.union(term2))==k])
    

    def get_minsup_set(self, transactions, itemset, count_set, min_sup):
        
        temp_set = defaultdict(int)
        new_set = defaultdict(int)

        for key,item in itemset.items():
            count_set[item] += sum([1 for trans in transactions if item.issubset(frozenset(trans))])
            temp_set[item] += sum([1 for trans in transactions if item.issubset(frozenset(trans))])
        
        n = len(transactions)
        # print("n",n)
        count = 0
        for item, cnt in temp_set.items():
            # if float(cnt)/n >= min_sup:
            if cnt >= min_sup:
                new_set[count] = item
                count+=1
        
        return new_set

        # def getSupport(self, item):
        # pass



def trans_red(transactions, Lk, Ck, k):
    non_freq_set = defaultdict(int)
    count = 0

    if k ==1:
        for key, item_set in Ck.items():
            if item_set not in Lk.values():
                non_freq_set[count] = item_set
                count += 1
        
        # print("non_freq_sets\n",non_freq_set)

        for key, item in non_freq_set.items():
            for trans in transactions:
                if item.issubset(frozenset(trans)):
                    trans.remove(list(item)[0])
                    # trans[1]=len(trans[0])
    
    remove_val= []

    for x in range(len(transactions)):
        if len(transactions[x]) == k:
            remove_val.append(transactions[x])

    for x in remove_val:
        transactions.remove(x)

    print("non_done")


my_apriori = Apriori(.2,3)

transactions= my_apriori.get_transactions("set_2.txt")

print(transactions)

item_sets_one = my_apriori.get_itemsets(transactions)

# print(item_sets_one)

count_set = defaultdict(int)

L1 = my_apriori.get_minsup_set(transactions, item_sets_one,count_set, 3)

print("C1\n", item_sets_one)
print("L1\n",L1)


k=1
Lk = L1

trans_red(transactions, Lk, item_sets_one, k)

print("after reduction")
print(transactions)


C2 = my_apriori.get_join_set(L1,2)

print(C2)


freq_set =dict()
# freq_set[0]=Lk
while len(Lk) !=0:
    freq_set[k] = Lk
    k+=1
    Ck = my_apriori.get_join_set(Lk, k)
    # print(type(Ck))
    Lk = my_apriori.get_minsup_set(transactions, Ck, count_set, 2)
    trans_red(transactions, Lk, Ck, k)
    # print(k, Ck, len(Lk))

print("frequent sets")
print(len(freq_set))
for key, val in freq_set.items():
    print(key, val)

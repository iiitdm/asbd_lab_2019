

from fim import sam, carpenter, relim, accretion, ista


def get_transactions(dataset):
        in_file = open(dataset, "r")
        if_lines = in_file.readlines()
        transactions = []
        for lines in if_lines[:50]:
            string_lines = lines.split()
            int_lines = list(map(int, string_lines))
            int_lines.sort()
            # order_set = OrderedSet(string_lines)
            transactions.append(int_lines)
        
        return transactions


transactions = get_transactions("kosarak.dat")

print('transactions:')
for t in transactions:
    print(t)

print  ('\nista supp=3, min_items_per_set=2')
for r in relim(transactions, supp=-3, zmin=2):
    print(r)

print  ('\ncarpenter supp=3, min_items_per_set=2')
for r in carpenter(transactions, supp=-3, zmin=2):
    print(r)

# print  ('\ncarpenter supp=3, min_items_per_set=2')
# for r in accretion(transactions, supp=-3, zmin=2):
#     print(r)
# import collection from defaultdict
from collections import defaultdict
from ordered_set import OrderedSet


class Apriori(object):

    def __init__(self, min_sup, min_conf):
        self.min_conf = min_conf
        self.min_sup = min_sup
    
    def get_transactions(self, dataset):
        in_file = open(dataset, "r")
        if_lines = in_file.readlines()
        transactions = []
        for lines in if_lines[:50]:
            string_lines = lines.split()
            # int_lines = list(map(int, string_lines))
            # int_lines.sort()
            order_set = OrderedSet(string_lines)
            transactions.append(order_set)

        
        return transactions
    
    def get_itemsets(self, transactions):

        itemset = defaultdict(int)
        count = 0
        for line in transactions:
            for item in line:
                itemset[count] = frozenset([item])
                count +=1

            # for item in line:
            #     itemset.add(frozenset([item]))
        
        return itemset
    
    def get_join_set(self, termSet, k):
        """ Generate new k-terms candiate itemset"""
        # print(type(termSet))

        local_set = defaultdict(int)
        count = 0

        for key1,term1 in termSet.items():
            for key2,term2 in termSet.items():
                if len(term1.union(term2)) ==k:
                    joinset = term1.union(term2)
                    if not joinset in local_set.values():
                        local_set[count] = joinset
                        count+=1
        # print("LOCAL SET")
        # print(local_set)

        return(local_set)
        # local_set

        # print()


        # return set([term1.union(term2) for key1,term1 in termSet.items() for key2,term2 in termSet.items() 
                    # if len(term1.union(term2))==k])
    

    def get_minsup_set(self, transactions, itemset, count_set, min_sup):
        
        temp_set = defaultdict(int)
        new_set = defaultdict(int)

        for key,item in itemset.items():
            count_set[item] += sum([1 for trans in transactions if item.issubset(frozenset(trans))])
            temp_set[item] += sum([1 for trans in transactions if item.issubset(frozenset(trans))])
        
        n = len(transactions)
        # print("n",n)
        count = 0
        for item, cnt in temp_set.items():
            # if float(cnt)/n >= min_sup:
            if cnt >= min_sup:
                new_set[count] = item
                count+=1
        
        return new_set

        # def getSupport(self, item):
        # pass




my_apriori = Apriori(.2,3)

transactions = my_apriori.get_transactions("my_set.txt")

print(transactions)

item_sets_one = my_apriori.get_itemsets(transactions)

# print(item_sets_one)

count_set = defaultdict(int)

L1 = my_apriori.get_minsup_set(transactions, item_sets_one,count_set, 2)

print("L1",L1)

k=1
Lk = L1

C2 = my_apriori.get_join_set(L1,2)

print(C2)


freq_set =dict()
# freq_set[0]=Lk
while len(Lk) !=0:
    freq_set[k] = Lk
    k+=1
    Ck = my_apriori.get_join_set(Lk, k)
    # print(type(Ck))
    Lk = my_apriori.get_minsup_set(transactions, Ck, count_set, 2)
    # print(k, Ck, len(Lk))

print("frequent sets")
print(len(freq_set))
for key, val in freq_set.items():
    print(key, val)

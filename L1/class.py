#!/bin/bash

import matplotlib.pyplot as plt
import numpy as np

objects = ('85-100', '70-85', '55-70', '<55')
objects = ('S', 'A', 'B', 'C')
y_pos = np.arange(len(objects))
performance = [31,29,25,15]

plt.subplot(2,1,1)

plt.bar(y_pos, performance, align='center', alpha=0.5)
plt.xticks(y_pos, objects)
plt.ylabel('Number of students')
plt.title('Grade brakeup')

plt.subplot(2,1,2)
explode = (0, 0, 0, 0)
colors = ['gold', 'yellowgreen', 'lightcoral', 'lightskyblue']
plt.pie(performance, explode=explode, labels=objects, colors=colors,
autopct='%1.1f%%', shadow=True, startangle=140)


plt.show()


#!/bin/bash

import numpy as np

arr = []

for i in range(1,19):
    arr.append((25+((i+7)%10)) if i%2 else (25+((i+8)%10)) )

print("mean:"+str(np.mean(arr))+ "  median:"+str(np.median(arr)))
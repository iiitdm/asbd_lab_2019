#!/bin/bash

import numpy as np

# creating the data
data_set = np.random.randint(1, 5, size=50)

# initializing the count
brown = 0
blue = 0
green = 0
other = 0

# getting the count
for x in data_set:
    if x == 1:
        brown +=1
    elif x == 2:
        blue += 1
    elif x == 3:
        green += 1
    else:
        other+=1

print("SCORE            TALLY\t\t\tFREQUENCY")
print("1=BROWN          {0}\t\t{1}".format('||||\\ '*int(brown/5)+'|'*(brown%5), brown ))
print("2=BLUE          {0}\t\t{1}".format('||||\\ '*int(blue/5)+'|'*(blue%5), blue ))
print("3=GREEN          {0}\t\t{1}".format('||||\\ '*int(green/5)+'|'*(green%5), green ))
print("4=OTHER          {0}\t\t{1}".format('||||\\ '*int(other/5)+'|'*(other%5), other ))

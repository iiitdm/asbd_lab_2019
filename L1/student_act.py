
#!/bin/bash

import matplotlib.pyplot as plt
import numpy as np


objects=('studying','playing','sleeping','hobby','f&f')
performance=[33, 18, 30, 5,14]

explode = (0, 0, 0, 0, 0)
colors = ['red','gold', 'yellowgreen', 'lightcoral', 'lightskyblue']
plt.pie(performance, explode=explode, labels=objects, colors=colors,
autopct='%1.1f%%', shadow=True, startangle=140)


plt.show()

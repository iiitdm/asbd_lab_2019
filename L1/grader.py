#!/bin/bash

import matplotlib.pyplot as plt
import numpy as np

in_file=open("marks.txt","r")

lines = in_file.readlines()

marks=[]
Ab_S=0
Ab_A=0
Ab_B=0
Ab_C=0
Ab_D=0
Ab_E=0
for line in lines:
    md_sem, end_sem, assign =map(int,line.split())
    if md_sem>30 or end_sem > 50 or assign>20:
        print("invalid marks breaking")
        in_file.close()
    marks.append(md_sem+end_sem+assign)

class_avg = np.mean(marks)

pass_std = 0
pass_sum = 0
#Absolute grading

for x in marks:
    #for passing students for absolute grading
    if x >= class_avg/2:
        pass_std += 1
        pass_sum += x
    if x >= 90:
        Ab_S +=1
    elif x >= 80:
        Ab_A +=1
    elif x >= 70:
        Ab_B +=1
    elif x >= 60:
        Ab_C+=1
    elif x >= 50:
        Ab_D+=1
    elif x <= class_avg/2:
        Ab_E+=1

pass_mean = x/pass_std
X = pass_mean - (class_avg/2)
max_mark = np.max(marks)
S_cutoff = max_mark - 0.1*(max_mark-pass_mean)
Y = S_cutoff - pass_mean
A_cutoff = pass_mean + Y*5/8
B_cutoff = pass_mean + Y*2/8
C_cutoff = pass_mean - X*2/8
D_cutoff = pass_mean - X*5/8
E_cutoff = class_avg/2



Re_S=0
Re_A=0
Re_B=0
Re_C=0
Re_D=0
Re_E=0

for x in marks:
    if x >= S_cutoff:
        Re_S +=1
    elif x >= A_cutoff:
        Re_A +=1
    elif x >= B_cutoff:
        Re_B +=1
    elif x >= C_cutoff:
        Re_C+=1
    elif x >= D_cutoff:
        Re_D+=1
    elif x <= E_cutoff:
        Re_E+=1



print("Grade\t\t Frequency")

print("S\t\t",Ab_S)
print("A\t\t",Ab_A)
print("B\t\t",Ab_B)
print("C\t\t",Ab_C)
print("D\t\t",Ab_D)
print("E\t\t",Ab_E)

print("Relative")

print("Grade\t\t Frequency")

print("S\t\t",Re_S)
print("A\t\t",Re_A)
print("B\t\t",Re_B)
print("C\t\t",Re_C)
print("D\t\t",Re_D)
print("E\t\t",Re_E)

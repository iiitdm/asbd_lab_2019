#!/bin/bash


from random import seed
from random import randint
import numpy as np

seed()
arr = []
for i in range(20):
    # y = 2x +3
    arr.append(2*randint(6, 100000) + 3)

print("standard deviation:{0}".format(np.std(arr)))


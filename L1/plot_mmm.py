#!/bin/bash

from matplotlib import pyplot as plt
from scipy import stats
import numpy as np
from statistics import mode
import seaborn as sns

df = [167.65, 167, 172, 175, 165, 167, 168, 167, 167.3, 170, 167.5, 170, 167, 169, 172]

f, (ax_box, ax_hist) = plt.subplots(2, sharex=True, gridspec_kw= {"height_ratios": (0.2, 1)})
mean=np.mean(df)
median=np.median(df)
mode=mode(df)
std=np.std(df)
skew=stats.skew(df)
print(skew, std)


sns.distplot(df, ax=ax_box)
#ax_box.axvline(std, color='y', linestyle='-')
#ax_box.axvline(skew, color='m', linestyle='-')

# sns.boxplot(df[" rating"], ax=ax_box)
# ax_box.axvline(mean, color='r', linestyle='--')
# ax_box.axvline(median, color='g', linestyle='-')
# ax_box.axvline(mode, color='b', linestyle='-')

sns.distplot(df, ax=ax_hist)
ax_hist.axvline(mean, color='r', linestyle='--')
ax_hist.axvline(median, color='g', linestyle='-')
ax_hist.axvline(mode, color='b', linestyle='-')
plt.legend({'Mean':mean,'Median':median,'Mode':mode})

# ax_box.set(xlabel='')
plt.show()
